#include "Arbre.h"
#include "Noeud.h"
#include <vector>

Arbre::Arbre(){}

Arbre::Arbre(string ch)
{
    int i=0;
    int taille=0;
    Noeud* q;
    vector <Noeud*> tab_adr;

    while(ch[i] != ';'){

        if((ch[i] >= 'a') && (ch[i] <= 'z')){
            taille++;
            q=new Noeud(ch[i],NULL,0,tab_adr);
            q->setAdresse(q);
            tab_adr.push_back(q);
            Matrix.push_back(q);

        }else if(ch[i]== ')'){
            i=i+1;
            q=new Noeud(ch[i],NULL,taille,tab_adr);
            q->setAdresse(q);
            for(int i=0;i<taille;i++){
                tab_adr[i]->changement_Adr_Parent(q);
            }
            taille=1;
            tab_adr.clear();
            tab_adr.push_back(q);
            Matrix.push_back(q);
        }

        i=i+1;
    }
    root=q;
}

Arbre::Arbre(const Arbre& w){
    root=w.root;
    for(int i=0; i<w.Matrix.size(); i++){

        Noeud* q=new Noeud(*w.Matrix[i]);
        Matrix.push_back(q);
    }
}

void Arbre::affiche(){

    for(int i=0;i<Matrix.size();i++){
        Matrix[i]->affiche();
        cout<<endl;
    }
    cout<<" l'adresse du root est: "<<root<<endl;
}

Arbre::~Arbre()
{
    //dtor
}
