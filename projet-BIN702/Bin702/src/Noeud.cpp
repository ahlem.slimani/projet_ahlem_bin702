#include "Noeud.h"

Noeud::Noeud(){}

Noeud::Noeud(char ch,Noeud* adr,int n,vector <Noeud*> tabAdr)
{
    label=ch;
    parent=adr;
    nbrFils=n;
    tab=tabAdr;
}

Noeud::Noeud(const Noeud& w){
    label=w.label;
    nbrFils=w.nbrFils;
    parent=w.parent;

    for(int i=0; i<nbrFils; i++){

        Noeud* q=new Noeud(*w.tab[i]);
        tab.push_back(q);
    }
}

void Noeud::changement_Adr_Parent(Noeud* adr)
{
    parent=adr;
}

void Noeud::affiche(){
    cout<<"Label: "<<label<<" et sont adresse: "<<adresse<<endl;

    if(parent != NULL)
    cout<<"Adresse de parent: "<<parent<<" Label de parent: "<<parent->label<<endl;

    cout<<"Nombre de Fils: "<<nbrFils<<endl;
    for(int i=0;i<nbrFils;i++){

        if(tab[i] != NULL)
            cout<<" Adresse de fils: "<<tab[i]<<" Label de fils: "<<tab[i]->label<<" | ";

    }
    cout<<endl;
}

Noeud::~Noeud()
{
    for(int i=0;i<tab.size(); i++) delete tab[i];
    tab.clear();
}
