#ifndef ARBRE_H
#define ARBRE_H
#include <iostream>
#include <string>
#include "Noeud.h"
using namespace std;

class Arbre
{   Noeud* root;
    vector<Noeud*> Matrix;
public:
    Arbre();
    Arbre(string);
    Arbre(const Arbre&);
    void affiche();
    virtual ~Arbre();
};
#endif // ARBRE_H
