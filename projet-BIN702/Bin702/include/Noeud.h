#ifndef NOEUD_H
#define NOEUD_H
#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Noeud
{
    char label;
    Noeud* adresse;
    Noeud* parent;
    int nbrFils;
    vector <Noeud*> tab;

public:
    Noeud();
    Noeud(char,Noeud*,int,vector <Noeud*>);
    void setAdresse(Noeud* a){adresse=a;}
    Noeud(const Noeud&);
    void changement_Adr_Parent(Noeud*);
    void affiche();
    virtual ~Noeud();

};
#endif // NOEUD_H
